-- MySQL Advances Queries and Joins
-- [SECTIONS] Add new records
-- Add 5 artist, at least 2 albums each, 1-2 songs per album.
-- Add 5 artists
-- Taylor Swift
-- Lady Gaga
-- Justine Biever
-- Ariana Grande
-- Bruno Mars
INSERT INTO
    artists (name)
VALUES
    ("Taylor Swift");

INSERT INTO
    artists (name)
VALUES
    ("Lady Gaga");

INSERT INTO
    artists (name)
VALUES
    ("Justine Biever");

INSERT INTO
    artists (name)
VALUES
    ("Ariana Grande");

INSERT INTO
    artists (name)
VALUES
    ("Bruno Mars");

---
INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Fearless", "2008-11-11", 3);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Red", "2012-10-22", 3);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("A Star is born", "2018-10-05", 4);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Born this way", "2011-05-23", 4);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Purpose", "2015-11-13", 5);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Believe", "2012-06-15", 5);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Dangerous Woman", "2016-05-20", 6);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Thank U, Next", "2019-02-08", 6);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("24k Magic", "2016-11-18", 7);

INSERT INTO
    albums (album_title, date_released, artist_id)
VALUES
    ("Earth to Mars", "2011-02-07", 7);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Fearless", 402, "Pop Rock", 3);

--
INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Love Story", 355, "Country Pop", 3);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    (
        "State of Grace",
        455,
        "Rock, Alternative Rock, Arena Rock",
        4
    );

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Red", 341, "Country", 4);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Black eyes", 304, "Rock and Roll", 5);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Shallow", 336, "Country, Rock, Folk Rock", 5);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Born this way", 420, "Electropop", 6);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    (
        "Sorry",
        320,
        "Dancehall-poptropical Housemoombahton",
        7
    );

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Boyfriend", 252, "pop", 8);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Into You", 405, "EDM House", 9);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Thank U, Next", 327, "Pop, R&B", 10);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("24K Magic", 346, "Funk, Disco", 11);

INSERT INTO
    songs (title, length, genre, album_id)
VALUES
    ("Lost", 321, "Pop", 12);

-- [SECTION] Advanced Selects
-- Exclude records
SELECT
    *
FROM
    songs
WHERE
    id != 11;

-- LIMIT: Show only specific number of records.
SELECT
    *
FROM
    songs
LIMIT
    5;

-- Greater than or equal/less than or equal (comparison)
SELECT
    *
FROM
    songs
WHERE
    id <= 11;

SELECT
    *
FROM
    songs
WHERE
    id >= 11;

-- Get specific records using OR
SELECT
    *
FROM
    songs
WHERE
    id = 1
    OR id = 5
    OR id = 9;

-- Get specific records using IN
-- a shorthand method for multiple OR conditions.
SELECT
    *
FROM
    songs
WHERE
    id IN (1, 5, 9);

SELECT
    *
FROM
    songs
WHERE
    genre IN ("Pop", "K-Pop");

-- Combining conditions
SELECT
    *
FROM
    songs
WHERE
    album_id = 4
    AND id < 8;

-- Find Partial matches.
-- Like is useed in a WHERE clause to search for a specified pattern in a column.
-- There are two wildcards used in conjunction with LIKE.
-- (%) - Represents zero, one or multiple characters.
-- (_) - Represents a single character. (2012-__-__)
SELECT
    *
FROM
    songs
WHERE
    title LIKE "%a";

-- Select keywords from the start;
SELECT
    *
FROM
    songs
WHERE
    title LIKE "a%";

-- Select keywords from the end;
SELECT
    *
FROM
    songs
WHERE
    title LIKE "%a%";

-- Select keyword ANYWHERE.
SELECT
    *
FROM
    albums
WHERE
    date_released LIKE "____-__-__";

SELECT
    *
FROM
    albums
WHERE
    date_released LIKE "201_-0_-0_";

-- get a record based on a specific pattern.
-- Sorting records (alphanumeric order: A-Z/Z-A)
-- Syntax SELECT * FROM table_name ORDER BY column_name ASC/DESC;
SELECT
    *
FROM
    songs
ORDER BY
    title ASC;

SELECT
    *
FROM
    songs
ORDER BY
    title DESC;

-- Getting distinct records.
-- Eliminates duplicate rows and display a unique list of values.
SELECT DISTINCT
    genre
FROM
    songs;

-- [SECTION] Table Joins
-- This is used to retrieve data from multiple tables.
-- This is performed whenever two or more tables are listed in a SQL statement.
-- https://joins.spathon.com/
-- Syntax of Inner Join (Join)
/*
Two Tables:

SELECT * (or column_name) FROM table1_name JOIN table2_name ON table1_column_id_pk = table2_column_id_fk;

Multiple Tables:
SELECT* (or column_name) FROM table1_name JOIN table2_name ON table1_column_id_pk = table2_column_id_fk JOIN table3_name ON table2_column_id_pk = table3_column_id_fk;
 */
-- Combine artists and albums
SELECT
    *
FROM
    artists
    JOIN albums ON artists.id = albums.artist_id;

-- Combine more than two tables.
SELECT
    *
FROM
    artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

-- Specify columns to be included in the Join table.
SELECT
    artists.name,
    albums.album_title,
    songs.title
FROM
    artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

-- Insert another artist
INSERT INTO
    artists (name) VALUE ("ED Sheeran");

--Show artist without records on the right side of the joined table.
SELECT
    artists.name,
    albums.album_title
FROM
    artists
    LEFT JOIN albums ON artists.id = albums.artist_id;

-- Right JOIN
SELECT
    artists.name,
    albums.album_title
FROM
    artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;

-- FULL OUTER JOIN
-- UNION: is used to combine the result set of two or more SELECT statements;
SELECT
    artists.name,
    albums.album_title
FROM
    artists
    LEFT JOIN albums ON artists.id = albums.artist_id
UNION
SELECT
    artists.name,
    albums.album_title
FROM
    artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;

SELECT
    artists.name,
    albums.album_title
FROM
    artists
    LEFT JOIN albums ON artists.id = albums.artist_id
UNION
SELECT
    artists.name,
    albums.album_title
FROM
    artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;